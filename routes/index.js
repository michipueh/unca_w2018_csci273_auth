var express = require('express');
var router = express.Router();
var {spawnSync} = require('child_process');
const spawn = require('cross-spawn');
var OAuth2Client = require('google-auth-library').OAuth2Client;
var client = new OAuth2Client("427115399226-vv9229r0rs77672hgtvupie1kumrt039.apps.googleusercontent.com");

router.get('/', function (req, res, next) {
  res.redirect('/register');
});

router.get('/register', function (req, res, next) {
  res.render('register', {page: 'Register', menuId: 'register', googleClientId: process.env.GOOGLE_CLIENT_ID});
});

router.post('/register', function (req, res, next) {
  let googleId = req.body.googleId;
  let password = req.body.password;
  let passwordRepeat = req.body.passwordRepeat;
  let messages = [];

  messages = [...messages, ...validateRegisterInput(googleId, password, passwordRepeat)];

  let promise = new Promise((resolve) => resolve());
  // no errors
  if (messages.length === 0) {
    promise = validateGoogleIdToken(googleId, messages)
      .then((ticket) => {
        const payload = ticket.getPayload();

        const emailSplit = payload.email.split("@");
        if(emailSplit.length !== 2) {
          messages.push({type: "danger", text: "Email is of wrong format. "});
          return;
        }
        const userid = emailSplit[0];
        const domain = payload.hd;
        const email = payload.email;
        const name = payload.name;
        if (domain !== "unca.edu") {
          messages.push({type: "danger", text: "No valid @unca.edu account was used to register. "});
        } else {
          const command = spawn.sync(process.env.SCRIPT_LOCATION, [userid, email, name, password], {
            shell: false,
            encoding: 'utf8'
          });
          // print errors
          if (command.stderr) {
            messages.push({type: "danger", text: command.stderr.toString()})
          }
          // print output
          if (command.stdout) {
            messages.push({type: "info", text: command.stdout.toString()});
          }
          // print success if no error
          if(command.status === 0) {
            messages.push({type: "success", text: "Perfect, we registered you!"});
          }
        }
      }).catch((error) => {
        if (!error.message) {
          error.message = "An exception occurred while verifying the login";
        }
        messages.push({type: "danger", text: error.message})
      });

  }

  Promise.resolve(promise)
    .finally(() => {
      res.render('register', {
        page: 'Register',
        menuId: 'register',
        messages: messages,
        googleClientId: process.env.GOOGLE_CLIENT_ID
      });
    }).catch(next);
});

async function validateGoogleIdToken(googleId) {
  return await client.verifyIdToken({
    idToken: googleId,
    audience: process.env.GOOGLE_CLIENT_ID
  }).then((ticket) => {
    let payload = ticket.getPayload();
    let domain = payload.hd;
    if (domain !== "unca.edu") {
      throw Error("Domain of the account has to be 'unca.edu'. ");
    }
    return ticket;
  });
}

function validateRegisterInput(googleId, password, passwordRepeat) {
  let messages = [];

  if (!googleId) {
    messages.push({
      type: "danger",
      text: "Please use the Google Sign In button before submitting."
    });
  }

  if (!password || !passwordRepeat) {
    messages.push({type: "danger", text: "Both passwords are required."});
  } else if (password !== passwordRepeat) {
    messages.push({type: "danger", text: "Passwords do not match."});
  }

  return messages;
}

module.exports = router;
