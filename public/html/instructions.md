# LDAP and Kerberos by Michael Pühringer

The following document is a manual on how to configure the latest Ubuntu server distribution to store users in LDAP and provide authentication via token using Kerberos. Additionally, all LDAP users will be authenticated using Kerberos (over SASL). Finally, a script is provided automating the creation of LDAP users and Kerberos principals. 

## Kerberos
> Sources: 
<https://help.ubuntu.com/community/Kerberos>  
<http://www.linux-mag.com/id/4738/>

__Install packages__  required for the server [All questions will be answered with Yes]: 
```
sudo apt-get install krb5-kdc krb5-admin-server -y
```

__Error handling__ if an error occurs during the installation about a failed realm creation, create it manually: 
```
kdb5_util create -s -r CS.UNCA.EDU
```

__Otherwise__ if no error occurs, reconfigure the package and create the realm [All questions will be answered with Yes]: 

```
sudo dpkg-reconfigure krb5-kdc
krb5_newrealm
```

__Manually add the realm__ may or may not be necessary, but sometimes you have to add the realm yourself in the ```/krb5.conf```: 

```
[realms] 
	...
	CS.UNCA.EDU = {
                kdc = canton.cs.unca.edu
                admin_server = canton.cs.unca.edu
                default_domain = canton.cs.unca.edu
        }

[domain_realm]
	...
	.unca.edu = CS.UNCA.EDU
	unca.edu = CS.UNCA.EDU
	
[logging]
    kdc = FILE:/var/log/krb5kdc.log
    admin_server = FILE:/var/log/kadmin.log
    default = FILE:/var/log/krb5lib.log
```

__Create a new administrator__ to avoid having to use ```kadmin.local``` using root every time: 

```
kadmin.local -q "addprinc admin/admin"
```

__Update the ACLs__ for better permission management in ```/etc/krb5kdc/kadm5.acl```: 

```
*/admin@CS.UNCA.EDU     *
*/*@CS.UNCA.EDU         i
*@CS.UNCA.EDU           i
```

__Finally__ restart the server and Kerberos should be working: 

```
service krb5-admin-server restart
service krb5-kdc restart
```

__Adding new users__ can be done from both the local and admin console now: 

```
kadmin -p admin/admin -q "add_principal mpuehrin"
# OR
kadmin.local -q "add_principal mpuehrin"
```

__Deleting users__ can be done from both the local and admin console now: 

```
kadmin -p admin/admin -q "delete_principal mpuehrin"
# OR
kadmin.local -q "delete_principal mpuehrin"
```

__Testing of a user__ can be done using the following commands: 

```
# Destroy the local tickets
kdestroy
# Retrieve a new ticket for admin/admin
kinit admin/admin
# List the issued tickets
klist
```

## LDAP 

__Install packages__ required for the server [All questions will be answered with Yes]: 

```
apt-get install slapd ldap-utils -y
```

__Creating the basic structure__ is required for the following configuration

```
# Start modifying the LDAP tree
ldapmodify -x -D cn=admin,dc=cs,dc=unca,dc=edu -W

# Create the OU "people"
dn: ou=people,dc=cs,dc=unca,dc=edu
changetype: add
ou: people
objectClass: top
objectClass: organizationalUnit
description: Parent object of all UNIX accounts

# Create the OU "groups"
dn: ou=groups,dc=cs,dc=unca,dc=edu
changetype: add
ou: groups
objectClass: top
objectClass: organizationalUnit
gidNumber: 100
description: Parent object of all UNIX groups

# Create the group "students"
dn: cn=students,ou=groups,dc=cs,dc=unca,dc=edu
changetype: add
objectclass: group
cn: dc=cs,dc=unca,dc=edu
description: Group of all students
```

## Integration with SASL
> Sources: 
<https://wiki.ubuntuusers.de/Kerberos/LDAP/>  <https://help.ubuntu.com/community/Kerberos#Integration_with_SASL>

SASL (Simple Authentication and Security Layer) is used to authenticate connection based protocols with any authentication method that the server and client both support. In our configuration, SASL will be used to enable the pass-through authentication of OpenLDAP, such that LDAP-users will be authenticated using the Kerberos backend. 

__Install packages__ required for the server [All questions will be answered with Yes]: 

```
sudo apt-get install sasl2-bin libsasl2-2 libsasl2-modules libsasl2-modules-gssapi-mit -y
```

__Add a new keytab__ for the LDAP server (Keytabs are principals for services, i.e. are stored in a file): 

```
kadmin.local

# Add principal for external LDAP-server (if exists)
addprinc -randkey ldap/ldap.cs.unca.edu
ktadd -k /etc/ldap/ldap.keytab ldap/ldap.cs.unca.edu
# Add principal for local LDAP-server (if exists)
addprinc -randkey ldap/canton.cs.unca.edu
ktadd -k /etc/ldap/ldap.keytab ldap/canton.cs.unca.edu
# Add principal for host of local server (required for saslauthd)
addprinc -randkey host/canton.cs.unca.edu
ktadd -k /etc/ldap/ldap.keytab host/canton.cs.unca.edu
exit 
```


__Update the permissions of the keytab__ such the the LDAP-server has access to it:  

```
chown openldap /etc/ldap/ldap.keytab
```

__Finally__ make sure that the LDAP-server uses this keytab in ```/etc/default/slapd```:

```
export KRB5_KTNAME=/etc/ldap/ldap.keytab
```

__Testing__ can be done using the sample client and server. Please follow [the procedure outlined here](https://help.ubuntu.com/community/Kerberos#Integration_with_SASL). 

## Add Kerberos auth to LDAP utils
> Source
<https://help.ubuntu.com/community/OpenLDAPServer#Kerberos_Authentication>

____ 

```
# Set the SASL host 
sudo ldapmodify -Q -Y EXTERNAL -H ldapi:/// <<EOF
dn: cn=config
changetype: modify
add: olcSaslHost
olcSaslHost: canton.cs.unca.edu
EOF

# Set the SASL realm
sudo ldapmodify -Q -Y EXTERNAL -H ldapi:/// <<EOF
dn: cn=config
add: olcSaslRealm
olcSaslRealm: CS.UNCA.EDU
EOF

# Set the regex mapping for users
sudo ldapmodify -Q -Y EXTERNAL -H ldapi:/// <<EOF
dn: cn=config
add: olcAuthzRegexp
olcAuthzRegexp: {0}"uid=([^/]*),cn=cs.unca.edu,cn=GSSAPI,cn=auth" "uid=$1,ou=people,dc=cs,dc=unca,dc=edu"
EOF

# Set the regex mapping for hosts
sudo ldapmodify -Q -Y EXTERNAL -H ldapi:/// <<EOF
dn: cn=config
add: olcAuthzRegexp
olcAuthzRegexp: {1}"uid=host/([^/]*).cs.unca.edu,cn=cs.unca.edu,cn=GSSAPI,cn=auth" "cn=$1,ou=hosts,dc=cs,dc=unca,dc=edu"
EOF

# Set the regex for the admin (ldap/admin principal)
sudo ldapmodify -Q -Y EXTERNAL -H ldapi:/// <<EOF
dn: cn=config
add: olcAuthzRegexp
olcAuthzRegexp: {2}"uid=ldap/admin,cn=cs.unca.edu,cn=GSSAPI,cn=auth" "cn=admin,dc=cs,dc=unca,dc=edu"
EOF
```

__Again__ make sure that the LDAP-server uses this keytab in ```/etc/default/slapd```:

```
export KRB5_KTNAME=/etc/ldap/ldap.keytab
```

__LDAP utils__ have to use Kerberos auth when the following is added to ```/etc/ldap/ldap.conf```

```
SASL_MECH GSSAPI
SASL_REALM CS.UNCA.EDU
```


__Test__ if LDAP utils is using Kerberos using the following command. You might have to issue a Kerberos ticket first. 

```
ldapsearch -Y GSSAPI
```

## Adding SASL Auth using Kerberos for LDAP users
> Source: 
<https://wiki.ubuntuusers.de/Kerberos/LDAP/#SASL-Kerberos-als-Passwordspeicher>

__Configure saslauthd__ to use Kerberos in ```/etc/default/saslauthd```: 

```
START=yes
...
...
...
MECHANISMS="kerberos5"
```
__Test saslauthd__ using: 

```
sudo testsaslauthd -u user -p password -r CS.UNCA.EDU -s ldap 
```

__For LDAP to use SASL__ for users, edit/create the file ```/etc/ldap/sasl2/slapd.conf```: 

```
pwcheck_method: saslauthd
saslauthd_path: /run/saslauthd/mux
```

__Edit permissions__ such that the LDAP-server can access the saslauthd socket: 

```
adduser openldap sasl
```

__Edit LDAP users__ such that the ```userPassword``` field has the format ```{SASL}user@REALM```: 
```
sudo ldapmodify -Q -Y EXTERNAL -H ldapi:/// <<EOF
dn: uid=mpuehrin,ou=people,dc=cs,dc=unca,dc=edu
replace: userPassword
userPassword: "{SASL}mpuehrin@CS.UNCA.EDU"
EOF
```

__Test LDAP__ using (user is a user in the LDAP directory people with a Kerberos principal): 

```
ldapwhoami -x -D "uid=userId,ou=people,dc=cs,dc=unca,dc=edu" -W
```

__Error fix__ if the authentication fails and ```service slapd status``` shows ```Permission denied```, follow <https://camratus.com/2017/01/24/openldap-lsc-active-directory-sync-and-login-pass-through/> for a quick fix: 

```
apt install apparmor-utils -y
aa-complain usr.sbin.slapd
/etc/init.d/slapd restart
service saslauthd restart
```

## Optional: SSL/TSL Encryption for LDAP
> Source:
<https://help.ubuntu.com/community/OpenLDAPServer#Encryption>

__Install packages__ required for SSL/TSL encryption and key creation: 

```
sudo apt install gnutls-bin ssl-cert -y
```

__Generate private key__ for CA: 

```
sudo sh -c "certtool --generate-privkey > /etc/ssl/private/cakey.pem"
```

__Create template__ for the CA at ```/etc/ssl/ca.info```: 

```
cn = UNCA CS Department
ca
cert_signing_key
```

__Create self-signed CA certificate__:

```
sudo certtool --generate-self-signed \
--load-privkey /etc/ssl/private/cakey.pem \
--template /etc/ssl/ca.info \
--outfile /etc/ssl/certs/cacert.pem
```

__Create private key for the LDAP-server__:

```
sudo certtool --generate-privkey \
--bits 1024 \
--outfile /etc/ssl/private/ldap_slapd_key.pem
```

__Create template__ for the LDAP-server at ```/etc/ssl/ldap.info```: 

```
organization = UNCA CS Department
cn = canton.cs.unca.edu
tls_www_server
encryption_key
signing_key
expiration_days = 3650
```

__Create certificate__ for the LDAP-server: 

```
sudo certtool --generate-certificate \
--load-privkey /etc/ssl/private/ldap_slapd_key.pem \
--load-ca-certificate /etc/ssl/certs/cacert.pem \
--load-ca-privkey /etc/ssl/private/cakey.pem \
--template /etc/ssl/ldap.info \
--outfile /etc/ssl/certs/ldap_slapd_cert.pem
```

__Adjust permissions__ to (only) allow the LDAP-server access to the key: 

```
sudo adduser openldap ssl-cert
sudo chgrp ssl-cert /etc/ssl/private
sudo chgrp ssl-cert /etc/ssl/private/ldap_slapd_key.pem
sudo chmod g+X /etc/ssl/private
sudo chmod g+r /etc/ssl/private/ldap_slapd_key.pem
```

__Configure the LDAP-server__ to find the CA-certificate at ````/etc/ldap/ldap.conf```:

```
TLS_CACERT /etc/ssl/certs/cacert.pem
```

__Configure the LDAP-server__ to use the certificate: 

```
sudo ldapmodify -Q -Y EXTERNAL -H ldapi:/// <<EOF
dn: cn=config
add: olcTLSCACertificateFile
olcTLSCACertificateFile: /etc/ssl/certs/cacert.pem
-
add: olcTLSCertificateFile
olcTLSCertificateFile: /etc/ssl/certs/ldap_slapd_cert.pem
-
add: olcTLSCertificateKeyFile
olcTLSCertificateKeyFile: /etc/ssl/private/ldap_slapd_key.pem
EOF
```

__Edit the start configuration__ of the LDAP-server to allow secure access in ```/etc/default/slapd```: 

```
SLAPD_SERVICES="ldap:/// ldaps:/// ldapi:///"
```

__Finally__ restart the LDAP-server: 

```
service slapd restart
```

## NodeJS and Website Setup

### NodeJS 10.x
> Source: 
<https://github.com/nodesource/distributions/blob/master/README.md>

__Install packages__ for NodeJS: 

```
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install -y nodejs
```

__Confirm__ installation by checking the version: 

```
nodejs --version
```

### Website

__Install git__ to close the website repository: 

```
apt-get install git -y
```

__Move to__ the folder where the website should be stored: 

```
cd /home/michipueh/
```

__Clone the repository__ (creates a new folder ```unca_w2018_csci273_auth```)

```
git clone https://michipueh@bitbucket.org/michipueh/unca_w2018_csci273_auth.git
```

__Move to__ the new folder: 

```
cd unca_w2018_csci273_auth
```

__Use NPM to install__ missing NodeJS packages: 

```
npm install
```

__Adjust permissions__ for the script to make it executable: 

```
chmod +x UNCAubuntuCreateLDAP-Kerberos
```

__Google-Sign-In-API Key__: The website relies on the Google-Sign-In-API and therefore requires a API key to work with. To obtain this key, please follow these instructions: <https://developers.google.com/identity/sign-in/web/sign-in#before_you_begin>. 

__Configure the website__ in the ```./.env``` file:

```
# Port of the application
PORT=80
# Google Client ID for the Sign-In-Button
GOOGLE_CLIENT_ID="427115399226-kdoa9kqtcv2ld13trpkfr5ugbatmhj6h.apps.googleusercontent.com"
# Script location 
SCRIPT_LOCATION="/home/michipueh/unca_w2018_csci273_auth/UNCAubuntuCreateLDAP-Kerberos"
```

__Test__ the website by running it (accessible on the defined port with any browser): 

```
npm start
```

#### Make the website a service to autostart it

__Create a service file__ at ```/etc/systemd/system/auth-web-app.service```: 

```
[Unit]
Description=Kerberos Auth Web Application

[Service]
ExecStart=/usr/bin/npm start
Restart=always
#User=user
#Group=nogroup
#Environment=PATH=/usr/bin:/usr/local/bin
#Environment=NODE_ENV=production
WorkingDirectory=/home/michipueh/unca_w2018_csci273_auth

[Install]
WantedBy=multi-user.target
```

__Enable__ and test the service: 

```
sudo systemctl daemon-reload
sudo systemctl enable auth-web-app.service
sudo systemctl start auth-web-app
sudo systemctl status auth-web-app
```

__Done!__

# Script: UNCAubuntuCreateLDAP-Kerberos 
The provided script has four parameters: 
1. UserId such as "mpuehrin"
2. Email such as "mpuehrin@unca.edu"
3. Full name such as "Michael Puehringer"
4. Password such as "12345678"

__Calling the script__ looks like this: 
```
./UNCAubuntuCreateLDAP-Kerberos "mpuehrin" "mpuehrin@unca.edu" "Michael Puehringer" "12345678"
```

__Tasks which are executed__ include the following: 
1. User ```mpuehrin``` is added to the LDAP ```people``` OU. 
2. A new home directory is created under ```/home/mpuehrin```. 
3. A new Kerberos principal is created with the name ```mpuehrin```. 